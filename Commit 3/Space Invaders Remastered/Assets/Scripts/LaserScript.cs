﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour {

    public GameObject Laser;
    Vector3 SpaceshipPosition;

    // Use this for initialization
    void Start()
    {

    }
	// Update is called once per frame
	void Update () {
        // Set Laser velocity
        transform.Translate(Vector3.up * 15f * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            cloneLaser();
        }

        // On collision
    }

    void cloneLaser()
    {
        // The following gets currently selected spaceship's position
        if (SpaceshipScript.spaceshipIndex == 1)
        { 
            SpaceshipPosition = GameObject.Find("Spaceship1").transform.position;
        }
        if (SpaceshipScript.spaceshipIndex == 2)
        {
            SpaceshipPosition = GameObject.Find("Spaceship2").transform.position;
        }

        if (SpaceshipScript.spaceshipIndex == 3)
        {
            SpaceshipPosition = GameObject.Find("Spaceship3").transform.position;
        }

        // Then instantiates a copy of the laser sprite to simulate a bullet
        Instantiate(Laser, SpaceshipPosition, transform.rotation);

    }

    void OnBecameInvisible() // Once the Laser moves out of the camera
    {
        
        // This was necessary since clones of clones can be created
        Destroy(GameObject.Find("Laser(Clone)")); 
    }
}
