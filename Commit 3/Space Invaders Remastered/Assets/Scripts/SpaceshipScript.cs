﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SpaceshipScript : MonoBehaviour {

    // This integer will change depending on the spaceship selected.
    // Index 1 = Wasp (selected by default)
    // Index 2 = Viking
    // Index 3 = Annihilator
    public static int spaceshipIndex = 1;
    public Text SelectedShipText;

    public void waspButtonDown()
    {
        spaceshipIndex = 1;
    }

    public void vikingButtonDown()
    {
        spaceshipIndex = 2;
    }

    public void annihilatorButtonDown()
    {
        spaceshipIndex = 3;
    }

    // Use this for initialization
    void Start () {
		Scene currentScene = SceneManager.GetActiveScene();

		GameObject Wasp = GameObject.Find("Spaceship1");
		GameObject Viking = GameObject.Find("Spaceship2");
		GameObject Annihilator = GameObject.Find("Spaceship3");

		// If one of the levels is loaded:
		if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Level1") || SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Level2") || SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Level3")) 
		{
			if (spaceshipIndex == 1) // If Wasp spaceship is selected in SpaceshipSelect Scene
			{
				Wasp.transform.position = new Vector3 (0.0f, -3.5f, -1f); // Transform its position to the bottom middle of the screen
				GameObject.Destroy (Viking); // And destroy the other spaceship gameobjects
				GameObject.Destroy (Annihilator);
			}

			if (spaceshipIndex == 2) 
			{
				Viking.transform.position = new Vector3 (0.0f, -3.5f, -1f);
				GameObject.Destroy (Wasp);
				GameObject.Destroy (Annihilator);
			}

			if (spaceshipIndex == 3) 
			{
				Annihilator.transform.position = new Vector3 (0.0f, -3.5f, -1f);
				GameObject.Destroy (Wasp);
				GameObject.Destroy (Viking);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {

		GameObject Wasp = GameObject.Find("Spaceship1");
		GameObject Viking = GameObject.Find("Spaceship2");
		GameObject Annihilator = GameObject.Find("Spaceship3");
		
		// If SpaceshipSelect scene is loaded
		if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("SpaceshipSelect")) {
			if (spaceshipIndex == 1) {
				SelectedShipText.text = "SELECTED SHIP: WASP"; // set text
			}

			if (spaceshipIndex == 2) {
				SelectedShipText.text = "SELECTED SHIP: VIKING";
			}

			if (spaceshipIndex == 3) {
				SelectedShipText.text = "SELECTED SHIP: ANNIHILATOR";
			}
		}
    }
}
