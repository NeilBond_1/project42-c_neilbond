﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    Rigidbody2D m_Rigidbody2D;
    float m_Speed;

    void Start()
    {
        //Fetch the RigidBody from the GameObject
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
        //Set the GameObject’s speed to 10
        m_Speed = 10.0f;
    }

    void Update()
    {
        //Press the Up arrow key to move the RigidBody upwards
        if (Input.GetKeyDown(KeyCode.A))
        {
            //Move RigidBody upwards
            m_Rigidbody2D.velocity = Vector2.left * m_Speed;
        }

        //Press the Down arrow key to move the RigidBody downwards
        if (Input.GetKeyDown(KeyCode.D))
        {
            //Move RigidBody downwards
            m_Rigidbody2D.velocity = Vector2.right * m_Speed;
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            //Move RigidBody upwards
            m_Rigidbody2D.velocity = Vector2.left * 0;
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            //Move RigidBody downwards
            m_Rigidbody2D.velocity = Vector2.right * 0;
        }
    }
}