﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InvaderScript : MonoBehaviour {
    // Find each invader's position
    public GameObject Invader1;
    public GameObject Invader2;
    public GameObject Invader3;
    public GameObject Invader4;

    // Use this for initialization
    void Start()
    {
        Vector3 Invader1Position = GameObject.Find("Invader1").transform.position;
        Vector3 Invader2Position = GameObject.Find("Invader2").transform.position;
        Vector3 Invader3Position = GameObject.Find("Invader3").transform.position;
        Vector3 Invader4Position = GameObject.Find("Invader4").transform.position;

        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level1"))
        {
            for (int i = 0; i < 14; i++)
            {
                Instantiate(Invader1, Invader1Position + (transform.right * i), transform.rotation);
                Instantiate(Invader2, Invader2Position + (transform.right * i), transform.rotation);
                Instantiate(Invader3, Invader3Position + (transform.right * i), transform.rotation);
                Instantiate(Invader4, Invader4Position + (transform.right * i), transform.rotation);

            }
        }
    }

	// Update is called once per frame
	void Update () {
		
	}
}
