﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StatsScript : MonoBehaviour {

    // Set health, score and timer
    public Text HealthText;
    public Text ScoreText;
    public Text HighscoreText;
    public Text TimerText;
    public Text BossHPText;

    // Use static variables throughout the game
    public static int SpaceshipHealth = 50;
    public static int Score = 0;
    public static int Highscore = 0;
    public static int bossHP = 0;
    int Timer = 30; // Is not static so that it can reset once level is changed

    // Use this for initialization
    void Start () {
        StartCoroutine(Countdown());
	}

    IEnumerator Countdown()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            Timer--;
        }
    }

	// Update is called once per frame
	void Update () {

        // Set health, score and timer texts
        HealthText.text = "HEALTH: " + SpaceshipHealth;
        ScoreText.text = "SCORE: " + Score;
        TimerText.text = "TIME: " + Timer;

        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level3"))
        {
            // Set boss health text
            BossHPText.text = "BOSS HEALTH: " + bossHP;
        }

        if (Timer <= 0)
        {
            if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level1"))
            {
                SceneManager.LoadScene(5);
                Timer = 30;
            }

            else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level2"))
            {
                SceneManager.LoadScene(6);
                Timer = 30;
            }

            else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level3"))
            {
                SceneManager.LoadScene(7);
                Destroy(this.gameObject); // Destroy score, health and timer layout
            }
        }

        if(SpaceshipHealth <= 0)
        {
            SceneManager.LoadScene(9);
        }
	}

    public void ResetVariables() // Reset variables upon exiting level through Quit, LoseScene or WinScene
    {
        SpaceshipHealth = 50;
        Score = 0;
        bossHP = 0;

        LaserScript.waitTime = 0.5f; // Resets to defaut shooting speed
    }
}
