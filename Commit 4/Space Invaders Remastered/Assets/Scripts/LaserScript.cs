﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour {

    public GameObject Laser;
    Vector3 SpaceshipPosition;

    // Use this for initialization
    void Start()
    {

    }

    #pragma strict

    public static float waitTime = 0.5f;
    private float timeStamp = Mathf.Infinity;

    // Update is called once per frame
    void Update() {

        // Set Laser velocity
        transform.Translate(Vector3.up * 15f * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            cloneLaser();
            timeStamp = Time.time + waitTime;
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            timeStamp = Mathf.Infinity;
        }

        if (Time.time >= timeStamp)
        {
            cloneLaser();
            Debug.Log("Firing");
            timeStamp = Time.time + waitTime;
        }
    }

    void cloneLaser()
    {
        // The following gets currently selected spaceship's position
        if (SpaceshipScript.spaceshipIndex == 1)
        { 
            SpaceshipPosition = GameObject.Find("Spaceship1").transform.position;
        }
        if (SpaceshipScript.spaceshipIndex == 2)
        {
            SpaceshipPosition = GameObject.Find("Spaceship2").transform.position;
        }

        if (SpaceshipScript.spaceshipIndex == 3)
        {
            SpaceshipPosition = GameObject.Find("Spaceship3").transform.position;
        }

        // Then instantiates a copy of the laser sprite to simulate a bullet
        Instantiate(Laser, SpaceshipPosition + (transform.up * 1), transform.rotation);
    }

    void OnBecameInvisible() // Once the Laser moves out of the camera
    {
        Destroy(GameObject.Find("Laser(Clone)"));
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(GameObject.Find("Laser(Clone)"));
    }
}
