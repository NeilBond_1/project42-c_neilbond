﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserInputScript : MonoBehaviour {

    public InputField NameField;
    public static string Username;

    void Start()
    {
        var input = gameObject.GetComponent<InputField>();
        var se = new InputField.SubmitEvent();
        se.AddListener(SubmitName);
        input.onEndEdit = se;

        //or simply use the line below, 
        //input.onEndEdit.AddListener(SubmitName);  // This also works
    }

    // Update is called once per frame
    void Update () {
        
    }

    private void SubmitName(string nameIn)
    {
        Username = nameIn;
    }
}
