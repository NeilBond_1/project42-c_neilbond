﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InvaderLaserSpawnerScript : MonoBehaviour {

    public GameObject InvaderLaser;
    Vector3 InvaderPosition;// Stores current gameobject's position
    public AudioSource InvaderLaserAudio;

    // Use this for initialization
    void Start () {

        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level1"))
        // Go to Coroutine to instantiate enemy lasers for level 1
        StartCoroutine(CloneInvaderLaser1());

        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level2"))
            // Go to Coroutine to instantiate enemy lasers for level 1
            StartCoroutine(CloneInvaderLaser2());
    }

    IEnumerator CloneInvaderLaser1() // For level 1
    {
        while (true)
        {
            yield return new WaitForSeconds(10);
            Instantiate(InvaderLaser, InvaderPosition + (transform.up * -1), transform.rotation);
            InvaderLaserAudio.Play();
        }
    }

    IEnumerator CloneInvaderLaser2() // For level 2
    {
        while (true)
        {
            yield return new WaitForSeconds(10);
            Instantiate(InvaderLaser, InvaderPosition + (transform.up * -1), transform.rotation);
            InvaderLaserAudio.Play();
            yield return new WaitForSeconds(4);
            Instantiate(InvaderLaser, InvaderPosition + (transform.up * -1), transform.rotation);
            InvaderLaserAudio.Play();
            yield return new WaitForSeconds(4);
            Instantiate(InvaderLaser, InvaderPosition + (transform.up * -1), transform.rotation);
            InvaderLaserAudio.Play();
        }
    }

    // Update is called once per frame
    void Update () {
        InvaderPosition = this.gameObject.transform.position;
    }
}
