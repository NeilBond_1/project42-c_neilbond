﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{

    Rigidbody2D m_Rigidbody2D;
    float m_Speed;

    public AudioSource EasterEgg;

    public bool isPaused;

    void Start()
    {
        //Used to pause the game
        bool isPaused = false;

        //Fetch the RigidBody from the GameObject
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
        //Set the GameObject’s speed to 10
        m_Speed = 10.0f;
    }

    void Update()
    {
        //For Pausing the game
        if (Input.GetKeyDown(KeyCode.P))
        {
            isPaused = !isPaused;

        }

        if (isPaused)
        {
            Time.timeScale = 0;
        }

        if (!isPaused)
        {
            Time.timeScale = 1;
        }

        //For player movement
        //Press the Up arrow key to move the RigidBody upwards
        if (Input.GetKeyDown(KeyCode.A))
        {
            //Move RigidBody upwards
            m_Rigidbody2D.velocity = Vector2.left * m_Speed;
        }

        //Press the Down arrow key to move the RigidBody downwards
        if (Input.GetKeyDown(KeyCode.D))
        {
            //Move RigidBody downwards
            m_Rigidbody2D.velocity = Vector2.right * m_Speed;
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            //Move RigidBody upwards
            m_Rigidbody2D.velocity = Vector2.left * 0;
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            //Move RigidBody downwards
            m_Rigidbody2D.velocity = Vector2.right * 0;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) //Easter egg script. sets fast shooting speed
    {
        if(SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level3"))
        {
            if (collision.gameObject == GameObject.Find("ColliderRight"))
            {
                LaserScript.waitTime = 0.05f;
                EasterEgg.Play();
            }
        }
    }
}