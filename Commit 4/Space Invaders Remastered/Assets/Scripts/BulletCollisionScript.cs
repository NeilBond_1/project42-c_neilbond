﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BulletCollisionScript : MonoBehaviour {

    public int health = 0; // General invader health

    void OnCollisionEnter2D(Collision2D col)
    {
        //Check for a match with the specified name on any GameObject that collides with your GameObject
        if (col.gameObject.name == "Laser(Clone)")
        {
            //If the GameObject's name matches the one you suggest, output this message in the console
            Debug.Log("ENEMY HIT! REDUCING HEALTH");
            health--;

            // Update player score in StatsScript
            StatsScript.Score++;
        }
    }

    // Use this for initialization
    void Start () {

        Scene currentLevel = SceneManager.GetActiveScene();

        if (currentLevel == SceneManager.GetSceneByName("Level1"))
        {
            health = 10;
        }

        if (currentLevel == SceneManager.GetSceneByName("Level2"))
        {
            health = 15;
        }

        if (currentLevel == SceneManager.GetSceneByName("Level3"))
        {
            health = 1000;
        }

        StartCoroutine(spriteMovement());
    }

    IEnumerator spriteMovement()
    {
        // Set sprite movement
        while (true)
        {
            this.gameObject.transform.position += Vector3.down * 0.2f;
            yield return new WaitForSeconds(1);
            this.gameObject.transform.position += Vector3.down * 0.2f;
            yield return new WaitForSeconds(1);
            this.gameObject.transform.position += Vector3.up * 0.2f;
            yield return new WaitForSeconds(1);
            this.gameObject.transform.position += Vector3.up * 0.2f;
            yield return new WaitForSeconds(1);
        }
    }

    // Update is called once per frame
    void Update() {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level3"))
        {
            StatsScript.bossHP = health;
        }
        

        if (health == 0)
        {
            Destroy(this.gameObject);
        }
    }
}
