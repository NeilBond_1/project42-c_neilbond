﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformScript : MonoBehaviour {

    int PlatformSpeed = 2;
    float PlatformLocationX;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {

        PlatformLocationX = this.gameObject.transform.position.x;

        transform.Translate(Vector3.right * PlatformSpeed * Time.deltaTime);

        if (PlatformLocationX >= 6)
        {
            PlatformSpeed = -2;
        } else if (PlatformLocationX <= -6)
        {
            PlatformSpeed = 2;
        }
    }

    
}
