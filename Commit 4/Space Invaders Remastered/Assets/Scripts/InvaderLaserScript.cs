﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InvaderLaserScript : MonoBehaviour {

    // Use this for initialization
    void Start () {
    
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.down * 2 * Time.deltaTime);
    }

    void OnBecameInvisible() // Once the Laser moves out of the camera
    {

        // This was necessary since clones of clones can be created
        Destroy(this.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject != GameObject.Find("Platform"))
        {
            if (collision.gameObject == GameObject.Find("Laser(Clone)"))
            {
                Destroy(this.gameObject);
            }
            else if (collision.gameObject != GameObject.Find("Laser(Clone)"))
            {
                if (CheatsScript.cheats == false) //If cheats are set to off
                {
                    StatsScript.SpaceshipHealth--;
                    Destroy(this.gameObject);
                }
                else
                {
                    Destroy(this.gameObject);
                }
            }
        }
        else
        {
            Destroy(this.gameObject);
        }
        
    }
}
