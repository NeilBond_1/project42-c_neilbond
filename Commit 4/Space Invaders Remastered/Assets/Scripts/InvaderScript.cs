﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InvaderScript : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        // Spawn Invaders
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level1"))
        {
            GameObject Invader1 = GameObject.Find("Invader1");
            GameObject Invader2 = GameObject.Find("Invader2");
            GameObject Invader3 = GameObject.Find("Invader3");
            GameObject Invader4 = GameObject.Find("Invader4");

            Vector3 Invader1Position = Invader1.transform.position; // Get original sprite position
            Vector3 Invader2Position = Invader2.transform.position;
            Vector3 Invader3Position = Invader3.transform.position;
            Vector3 Invader4Position = Invader4.transform.position;

            for (int i = 0; i < 14; i++) // Instantiate to right for 13 times
            {
                Instantiate(Invader1, Invader1Position + (transform.right * i), transform.rotation);
                Instantiate(Invader2, Invader2Position + (transform.right * i), transform.rotation);
                Instantiate(Invader3, Invader3Position + (transform.right * i), transform.rotation);
                Instantiate(Invader4, Invader4Position + (transform.right * i), transform.rotation);
            }
        }

        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level2"))
        {
            GameObject Invader5 = GameObject.Find("Invader5");
            GameObject Invader6 = GameObject.Find("Invader6");
            GameObject Invader7 = GameObject.Find("Invader7");
            GameObject Invader8 = GameObject.Find("Invader8");

            Vector3 Invader1Position = Invader5.transform.position; // Get original sprite position
            Vector3 Invader2Position = Invader6.transform.position;
            Vector3 Invader3Position = Invader7.transform.position;
            Vector3 Invader4Position = Invader8.transform.position;

            for (int i = 0; i < 14; i++) // Instantiate to right for 13 times
            {
                Instantiate(Invader5, Invader1Position + (transform.right * i), transform.rotation);
                Instantiate(Invader6, Invader2Position + (transform.right * i), transform.rotation);
                Instantiate(Invader7, Invader3Position + (transform.right * i), transform.rotation);
                Instantiate(Invader8, Invader4Position + (transform.right * i), transform.rotation);
            }
        }
    }

	// Update is called once per frame
	void Update () {

    }
}
