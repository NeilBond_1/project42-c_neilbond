﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheatsScript : MonoBehaviour {

    public Text HighscoreText;

    //Used to enable cheats
    public static bool cheats = false;

    public void cheat()
    {
        cheats = !cheats;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        HighscoreText.text = "HIGHSCORE = " + StatsScript.Highscore;

        if (cheats == true)
        {
            GameObject.Find("CheatsButton").GetComponentInChildren<Text>().text = "INVINCIBILITY = ON";
        }
        else if (cheats == false)
        {
            GameObject.Find("CheatsButton").GetComponentInChildren<Text>().text = "INVINCIBILITY = OFF";
        }
    }
}
