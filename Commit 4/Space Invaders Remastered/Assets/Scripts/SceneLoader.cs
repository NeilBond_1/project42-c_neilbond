﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

    // Main menu button methods:
    public void loadMenu()
    {
        SceneManager.LoadScene(0); // Loads the Menu scene
    }

    public void loadLevelSelector() // This function will be triggered when the player presses the select level button
    {
        SceneManager.LoadScene(1); // Loads the LevelSelector scene
    }

    public void chooseShip()
    {
        SceneManager.LoadScene(2); // Loads the SpaceshipSelect scene
    }

    public void instructions()
    {
        SceneManager.LoadScene(3); // Loads the Instructions scene
    }

    public void username()
    {
        SceneManager.LoadScene(8); // Loads the UserInput scene
    }

    public void credits()
    {
        SceneManager.LoadScene(10); // Loads the credits scene
    }

    // LevelSelector methods:
    public void Button1() // This function will activate when Level1Button is pressed in LevelSelector
    {
        SceneManager.LoadScene(4); // Loads Level1
    }

    public void Button2() // This function will activate when Level2Button is pressed in LevelSelector
    {
        SceneManager.LoadScene(5); // Loads Level2
    }

    public void Button3() // This function will activate when Level3Button is pressed in LevelSelector
    {
        SceneManager.LoadScene(6); // Loads Level3
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}
}
