﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndSceneScript : MonoBehaviour {

    public Text EndScoreText;

	// Use this for initialization
	void Start () {
        StatsScript.Score = StatsScript.Score + StatsScript.SpaceshipHealth; //Add player health to score

        if (StatsScript.Score > StatsScript.Highscore) //If score obtained is greater than current highscore
        {
            StatsScript.Highscore = StatsScript.Score; //Set new highscore
        }
    }
	
	// Update is called once per frame
	void Update () {
        EndScoreText.text = "CONGRATULATIONS! YOUR SCORE IS " + StatsScript.Score;
    }
}
