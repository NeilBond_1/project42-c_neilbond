﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupScript : MonoBehaviour {

    public AudioSource PowerupSound;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.down * 3f * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (this.gameObject == GameObject.Find("Attack-Speed-Powerup")) { 
            Destroy(this.gameObject);
            LaserScript.waitTime = LaserScript.waitTime - 0.1f;
        }
        else
        {
            Destroy(this.gameObject);
            StatsScript.SpaceshipHealth += 50;
        }

        PowerupSound.Play();
    }
}
