﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScript : MonoBehaviour {
    public AudioClip[] playlist;
    public AudioSource audioSource;

    // Use this for initialization
    void Start () {
        audioSource = FindObjectOfType<AudioSource>();
        audioSource.loop = false;

        DontDestroyOnLoad(gameObject);
    }

    private AudioClip GetRandomSong()
    {
        return playlist[Random.Range(0, playlist.Length)];
    }

	// Update is called once per frame
	void Update () {
		if (!audioSource.isPlaying)
        {
            audioSource.clip = GetRandomSong();
            audioSource.Play();
        }
	}

    bool isMute;

    public void Mute() // Used to mute audio
    {
        isMute = !isMute;
        AudioListener.volume = isMute ? 0 : 1;
    }

    public void Shuffle() // Used to shuffle audio
    {
        if (audioSource.isPlaying)
        {
            audioSource.clip = GetRandomSong();
            audioSource.Play();
        }
    }
}
