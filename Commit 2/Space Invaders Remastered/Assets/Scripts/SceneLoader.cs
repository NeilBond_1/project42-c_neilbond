﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

    // Main menu button methods:
    public void loadLevel() // This function starts on start button click and once player scores are reached
    {
        // The following line gets the current scene's ID
        int currentScene = SceneManager.GetActiveScene().buildIndex;

        if (currentScene == 0) // 0 refers to the Menu level ID.
        {
            SceneManager.LoadScene(3); // Load first level via level ID
        }

    }

    public void loadMenu()
    {
        SceneManager.LoadScene(0); // Loads the Menu scene
    }

    public void loadLevelSelector() // This function will be triggered when the player presses the select level button
    {
        SceneManager.LoadScene(1); // Loads the LevelSelector scene
    }

    public void chooseShip()
    {
        SceneManager.LoadScene(2); // Loads the SpaceshipSelect scene
    }

    // LevelSelector methods:
    public void Button1() // This function will activate when Level1Button is pressed in LevelSelector
    {
        SceneManager.LoadScene(3); // Loads Level1
    }

    public void Button2() // This function will activate when Level2Button is pressed in LevelSelector
    {
        SceneManager.LoadScene(4); // Loads Level2
    }

    public void Button3() // This function will activate when Level3Button is pressed in LevelSelector
    {
        SceneManager.LoadScene(5); // Loads Level3
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}
}
