﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpaceshipScript : MonoBehaviour {

    // This integer will change depending on the spaceship selected.
    // Index 1 = Wasp (selected by default)
    // Index 2 = Viking
    // Index 3 = Annihilator
    public static int spaceshipIndex = 1;
    public Text SelectedShipText;

    public void waspButtonDown()
    {
        spaceshipIndex = 1;
    }

    public void vikingButtonDown()
    {
        spaceshipIndex = 2;
    }

    public void annihilatorButtonDown()
    {
        spaceshipIndex = 3;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(spaceshipIndex == 1)
        {
            SelectedShipText.text = "SELECTED SHIP: WASP";
        }

        if (spaceshipIndex == 2)
        {
            SelectedShipText.text = "SELECTED SHIP: VIKING";
        }

        if (spaceshipIndex == 3)
        {
            SelectedShipText.text = "SELECTED SHIP: ANNIHILATOR";
        }
    }
}
